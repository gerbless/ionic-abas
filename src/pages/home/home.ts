import { Component } from '@angular/core';
import { NavController,Platform } from 'ionic-angular';

import { HTTP } from '@ionic-native/http';
import { File } from '@ionic-native/file';
import { Media, MediaObject } from '@ionic-native/media';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  audioList: any[] = [];
  fileName: string;
  filePath: string;
  audio: MediaObject;
  recording: boolean = false;
  base64Image:string;
  procentaje:number;
  currentItems: Array<any>
  urlApi:string="http://10.10.11.163:5000"
  server:string="http://35.226.227.204"
  headers : any;

  constructor(public navCtrl: NavController,
    private http: HTTP,
    private file: File,
    private media: Media,
    public platform: Platform,
  ) {
      this.headers = {headers: {'Content-Type': 'application/json'}}
  }
  startRecord() {

    this.procentaje=0;
    if (this.platform.is('ios')) {
      this.fileName = 'record'+new Date().getDate()+new Date().getMonth()+new Date().getFullYear()+new Date().getHours()+new Date().getMinutes()+new Date().getSeconds()+'.aac';
      this.filePath = this.file.documentsDirectory.replace(/file:\/\//g, '') + this.fileName;
      this.audio = this.media.create(this.filePath);
    } else if (this.platform.is('android')) {
      this.fileName = 'record'+new Date().getDate()+new Date().getMonth()+new Date().getFullYear()+new Date().getHours()+new Date().getMinutes()+new Date().getSeconds()+'.aac';
      this.filePath = this.file.externalDataDirectory + this.fileName;
      this.audio = this.media.create(this.filePath);
    }
    this.audio.startRecord();
    this.recording = true;
    window.setTimeout(() => this.stopRecord(), 3000);
  }

  stopRecord() {
    this.audio.stopRecord();
    this.recording=false;
    this.UploadAudio(this.fileName);
  }

  UploadAudio(file){

    this.procentaje=2
    this.file.checkFile(this.file.externalDataDirectory, file).then((data)=>{
      this.file.resolveLocalFilesystemUrl(this.filePath).then((newUrl)=>{
         let dirPath = newUrl.nativeURL;
         let dirPathSegments = dirPath.split('/');
         dirPathSegments.pop();
         dirPath = dirPathSegments.join('/');
         this.http.setHeader(this.server, 'Content-Type','multipart/form-data')
         this.http.uploadFile(`${this.server}/upload`,null,null,`${dirPath}/${newUrl.name}`,'audio')
         .then((res)=>{
          this.procentaje=1;
          this.currentItems = res.data.split(",");
         })

         .catch((err)=>{
          this.procentaje=1
           alert('Ocurrio un Error')
           //console.log(JSON.stringify(err, Object.getOwnPropertyNames(err)))
          //this.procentaje=JSON.stringify(err, Object.getOwnPropertyNames(err))
          //alert(JSON.stringify(err, Object.getOwnPropertyNames(err)))
        })
      })
   })

   .catch((err)=>{
     console.log(err);
   })
  }

}
