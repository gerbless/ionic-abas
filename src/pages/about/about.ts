import { Component } from '@angular/core';
import { NavController,Platform } from 'ionic-angular';

import { HTTP } from '@ionic-native/http';
import { File } from '@ionic-native/file';
import { Media, MediaObject } from '@ionic-native/media';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { DetectDeviceProvider } from '../../providers/detect-device/detect-device'

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  audioList: any[] = [];
  fileName: string;
  filePath: string;
  audio: MediaObject;
  recording: boolean = false;
  base64Image:string;
  procentaje:number;
  currentItems: Array<any>
  urlApi:string="http://10.10.11.163:5000"
  server:string="http://35.226.227.204"
  headers : any;
  uuid:string;
  deviceUuId:any =[];
  datafireBd: boolean = false;

  constructor(public navCtrl: NavController,
    private http: HTTP,
    private file: File,
    private media: Media,
    public platform: Platform,
    private uniqueDeviceID: UniqueDeviceID,
    private deviceSrvBD:DetectDeviceProvider) {
      this.headers = {headers: {'Content-Type': 'application/json'}}
      this.uuId()
  }

  uuId(){
    this.uniqueDeviceID.get()
    .then((uuid: any) => {
      this.uuid=uuid
      this.deviceConsult(uuid)
    })
    .catch((error: any) => console.log('no se pudo capturar el uuId del dispositivo',error))
  }
  startRecord() {

    this.procentaje=0;
    if (this.platform.is('ios')) {
      this.fileName = 'record'+new Date().getDate()+new Date().getMonth()+new Date().getFullYear()+new Date().getHours()+new Date().getMinutes()+new Date().getSeconds()+'.aac';
      this.filePath = this.file.documentsDirectory.replace(/file:\/\//g, '') + this.fileName;
      this.audio = this.media.create(this.filePath);
    } else if (this.platform.is('android')) {
      this.fileName = 'record'+new Date().getDate()+new Date().getMonth()+new Date().getFullYear()+new Date().getHours()+new Date().getMinutes()+new Date().getSeconds()+'.aac';
      this.filePath = this.file.externalDataDirectory + this.fileName;
      this.audio = this.media.create(this.filePath);
    }
    this.audio.startRecord();
    this.recording = true;
    window.setTimeout(() => this.stopRecord(), 3000);
  }

  stopRecord() {
    this.audio.stopRecord();
    this.recording=false;
    this.UploadAudio(this.fileName);
  }

  UploadAudio(file){

    this.procentaje=2
    this.file.checkFile(this.file.externalDataDirectory, file).then((data)=>{
      this.file.resolveLocalFilesystemUrl(this.filePath).then((newUrl)=>{
         let dirPath = newUrl.nativeURL;
         let dirPathSegments = dirPath.split('/');
         dirPathSegments.pop();
         dirPath = dirPathSegments.join('/');
         let params:object={
            uuId:this.uuid
         }
         this.deviceCreate()
         this.http.setHeader(this.server, 'Content-Type','multipart/form-data')
         this.http.uploadFile(`${this.server}/multi_audio`,params,null,`${dirPath}/${newUrl.name}`,'audio')
         .then((res)=>{
          this.procentaje=1;
          this.currentItems = res.data.split(",");
         })

         .catch((err)=>{
          this.deviceSrvBD.deleteDevice(this.deviceUuId)
          this.procentaje=1
           alert('Ocurrio un Error, Intente de nuevo')
           //console.log(JSON.stringify(err, Object.getOwnPropertyNames(err)))
          //this.procentaje=JSON.stringify(err, Object.getOwnPropertyNames(err))
          //alert(JSON.stringify(err, Object.getOwnPropertyNames(err)))
        })
      })
   })

   .catch((err)=>{
     console.log(err);
   })
  }

  deviceConsult(uuid){
    this.deviceSrvBD.getDevice(uuid).valueChanges()
    .subscribe((device) => {
      if(JSON.stringify(device)!= 'null'){
        this.datafireBd = true;
        this.deviceUuId = device;
      }else{
        this.datafireBd = false;
      }
    })
  }

  deviceCreate(){
    if(!this.datafireBd){
      this.deviceUuId ={
        id:this.uuid,
        device:this.uuid,
        audioname:[this.fileName],
        toke:1
      }
    }else{
      this.deviceUuId.audioname.push(this.fileName)
      this.deviceUuId.toke = ++this.deviceUuId.toke
    }
    this.deviceSrvBD.createDevice(this.deviceUuId)
  }

}
