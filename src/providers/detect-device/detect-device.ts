import { Injectable } from '@angular/core';
import { AngularFireDatabase } from "angularfire2/database";

/*
  Generated class for the DetectDeviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DetectDeviceProvider {

  constructor(public afDB:AngularFireDatabase) {

  }

  public  createDevice(device:any){
    return this.afDB.database.ref('/devices/' + device.id).set(device);
  }

  public getDevice(id){
    return this.afDB.object('/devices/' + id);
  }

  public deleteDevice(device:any){
    return this.afDB.database.ref('/devices/' + device.id).remove();
   }


}
