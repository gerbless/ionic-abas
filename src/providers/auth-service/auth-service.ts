
import { AngularFireDatabase } from 'angularfire2/database/database';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';


@Injectable()
export class AuthServiceProvider{
 constructor(public afBD:AngularFireDatabase, public afAuth:AngularFireAuth){
 }

 loginWithFacebook(){
   return this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider);
 }

 logout(){
   return this.afAuth.auth.signOut();
 }
}
