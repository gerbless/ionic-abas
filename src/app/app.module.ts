import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//librerías
import { HTTP } from '@ionic-native/http';
import { File } from '@ionic-native/file';
import { Media } from '@ionic-native/media';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
//para manejar firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth'
//providers
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { DetectDeviceProvider } from '../providers/detect-device/detect-device';

// configuración de acceso a la api firebase
export const firebaseConfig = {
  apiKey: "AIzaSyCqndMqIYfBMZRO7kmdWM-b9iHd4s7t6s8",
  authDomain: "ionic-crud-basico.firebaseapp.com",
  databaseURL: "https://ionic-crud-basico.firebaseio.com",
  projectId: "ionic-crud-basico",
  storageBucket: "ionic-crud-basico.appspot.com",
  messagingSenderId: "444272494358"
};

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage
  ],
  providers: [
    HTTP,
    File,
    Media,
    UniqueDeviceID,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider,
    DetectDeviceProvider
  ]
})
export class AppModule {}
